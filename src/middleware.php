<?php
// Application middleware
use Slim\App;
// e.g: $app->add(new \Slim\Csrf\Guard);

    //Midleware untuk validasi jwt
    //Ambil Data jwt Dari Setting
    $container = $app->getContainer();
    $settings = $container->get('settings');
    //************************************//

    $app->add(new Slim\Middleware\JwtAuthentication([
        "path"=> '/api',
        "secure"=>false,
        "attribute"=>"decoded_token_data", 
        "header" => "X-Token",
        "regexp" => "/(.*)/",            
        "algorithm"=>["HS256"],
        "secret"=>$settings['jwt']['secret'],        
        "error"=>function($req, $res, $args) {
            $data["status"] = "error";
            $data["message"] = $args["message"];
            return $res
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        }
    ]));

    